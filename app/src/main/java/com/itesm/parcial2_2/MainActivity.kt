package com.itesm.parcial2_2

import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private val TAG = "requestita"
    private lateinit var queue : RequestQueue
    private lateinit var imageView : ImageView
    private lateinit var currentURI : String

    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ result ->

        // obtener un thumbnail
        val image = result.data?.extras?.get("data") as Bitmap
        imageView.setImageBitmap(image)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageView = findViewById(R.id.imageView)

        // instanciar el queue
        queue = Volley.newRequestQueue(this)

        var url = "https://www.google.com"

        var stringRequest = StringRequest(
            Request.Method.GET,
            url,
            { response ->
                Toast.makeText(
                    this,
                    "response: $response",
                    Toast.LENGTH_SHORT)
                    .show()
            },
            { error ->
                Toast.makeText(
                    this,
                    "error: $error",
                    Toast.LENGTH_SHORT)
                    .show()
            }
        )

        stringRequest.tag = TAG
        //queue.add(stringRequest)

        // 2 contenedores importantes
        // objetos - {}
        // arreglos - []
        val jsonTest = "{'nombre': 'Juan', 'edad': 20}"
        val jsonTest2 = "{'nombre': 'Pedro', 'calificaciones': [89, 70, 34, 45]}"
        val jsonTest3 = "[{'nombre': 'Juan', 'edad': 20}, {'nombre': 'Pedro', 'edad': 21}, {'nombre': 'Maria', 'edad': 19}]"

        try{
            // parsing - interpretación de un json
            val objeto = JSONObject(jsonTest)
            Log.wtf("JSON", objeto.getString("nombre"))
            Log.wtf("JSON", objeto.getInt("edad").toString())

            val objeto2 = JSONObject(jsonTest2)
            val calificaciones = objeto2.getJSONArray("calificaciones")
            for(i in 0 until calificaciones.length()){
                Log.wtf("JSON", calificaciones.getInt(i).toString())
            }

            val objeto3 = JSONArray(jsonTest3)
            for(i in 0 until objeto3.length()){

                val actual = objeto3.getJSONObject(i)
                Log.wtf("JSON", actual.getString("nombre"))
                Log.wtf("JSON", actual.getInt("edad").toString())
            }

        } catch(e : JSONException) {
            e.printStackTrace()
        }

        //JSONArrayRequest
        //JSONObjectRequest

        url = "https://corona.lmao.ninja/v2/countries/"
        val jsonArrayRequest = JsonArrayRequest(
            Request.Method.GET,
            url,
            null,
            { response ->

                for(i in 0 until response.length()) {

                    val actual = response.getJSONObject(i)
                    val countryInfo = actual.getJSONObject("countryInfo")
                    Log.wtf("PAISES", countryInfo.getString("iso3"))
                }
            },
            { error ->

                Toast.makeText(this, "error: $error", Toast.LENGTH_SHORT).show()
            }
        )

        queue.add(jsonArrayRequest)

    }

    override fun onStop() {
        super.onStop()

        queue.cancelAll(TAG)
    }

    public fun showToast(view : View){

        Toast.makeText(this, getString(R.string.toast_text), Toast.LENGTH_SHORT).show();
    }

    public fun tomarFoto(view : View){

        val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        resultLauncher.launch(takePhotoIntent)
    }
}